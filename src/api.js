import { useFetch } from 'react-async';
import sqltomango from 'sqltomango';

const host = window.location.host.split('.')[0];
const apiurl = 'https://rudixauth.herokuapp.com/test/api/';
const toUrl = json => {
  const checkForKey = json.key ? { key: `"${json.key}"` } : {};
  const keys = json.keys ? `/${json.keys}` : '';
  const checkForStartKey = json.start_key ? { start_key: `"${json.start_key}"` } : {};
  const params = Object.keys({ ...json, ...checkForKey, ...checkForStartKey })
    .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(json[k])}`)
    .join('&');
  return `${db.apiurl}${json.db}/${json.view}${keys}?${params}`;
};

const db = {
  apiurl,
  toUrl,
  q: async (id, dbId) => {
    const json = sqltomango.parse(id);
    json.update = false;
    const config = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(json),
    };
    const response = await fetch(`${apiurl}${dbId}/_find`, config);
    const jsonx = await response.json();

    return new Promise(resolve => {
      resolve(host === 'mac' ? { ...jsonx, mango: json } : jsonx);
    });
  },

  get: json =>
    useFetch(toUrl(json), {
      headers: { Accept: 'application/json' },
    }),
  getUrl: url =>
    useFetch(url, {
      headers: { Accept: 'application/json' },
    }),
  bulkGet: async (arr, dbId, limit) => {
    const get = await fetch(`${apiurl}${dbId}/_bulk_get`, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify({
        docs: arr,
        update: false,
        limit: limit || 100,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const resp = await get.json();
    const formatted = resp.results.map(item => item.docs[0].ok);
    return new Promise(resolve => {
      resolve(formatted);
    });
  },
};

export default db;
