export const apps =  [
    {
      title: 'Библиотека ',
      img:
        'https://gitlab.com/arpecop/reactapp/raw/master/public/fb/reactapps/1.png',
      description: 'Офлайн четец на разпространявани книги от популярни сайтове',
      id:'com.rudixlabs.bookz',
    },
    {
      title: 'Вицове',
      img:
        'https://gitlab.com/arpecop/reactapp/raw/master/public/fb/reactapps/2.png',
      description: 'Над 50 000 вица от различни категори с възможност за добавяне на любими',
      id:'com.rudixlabs.jokes2'
    },
  
    {
      title: 'Виждам те Кат',
      img:
        'https://gitlab.com/arpecop/reactapp/raw/master/public/fb/reactapps/4.png',
      description: '  ',
      id:'com.rudixlabs.vijdamte'
    },
    {
      title: 'Съновник',
      img:
        'https://gitlab.com/arpecop/reactapp/raw/master/public/fb/reactapps/7.png',
      description: '  ',
    },
    {
      title: 'Четиво',
      img:
        'https://gitlab.com/arpecop/reactapp/raw/master/public/fb/reactapps/8.png',
      description: '  ',
      id:'com.rudixlabs.chetiva'
    } 
  ],
