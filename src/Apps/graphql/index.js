import React, { Component } from "react";
import { createTeleporter } from "react-teleporter";
// import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: "https://vm8mjvrnv3.lp.gql.zone/graphql"
});
const StatusBar = createTeleporter();

const Page = () => {
  return <StatusBar.Source>Loading...1111</StatusBar.Source>;
};
const Header = () => {
  return (
  <div>
      header wrapper
      <h1>
        <StatusBar.Target />
      </h1>
      end wrapper
    </div>
  );
};

function App() {
  return (
    <div>
      <Page />
      <Header />
    </div>
  );
}

export default App;
