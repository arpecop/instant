import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Facebook } from '../../Facebook';
import Canvas from './Canvas';

console.log('test');

const FB = new Facebook();
function toDataURL(src, callback, outputFormat) {
  const img = new Image();
  img.crossOrigin = 'Anonymous';
  img.onload = function () {
    const canvas = document.createElement('CANVAS');
    const ctx = canvas.getContext('2d');
    let dataURL;
    canvas.height = this.naturalHeight;
    canvas.width = this.naturalWidth;
    ctx.drawImage(this, 0, 0);
    dataURL = canvas.toDataURL(outputFormat);
    callback(dataURL);
  };
  img.src = src;
  if (img.complete || img.complete === undefined) {
    img.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';
    img.src = src;
  }
}

class Banica extends Component {
  constructor(props) {
    super();
    this.state = {
      h: window.innerHeight,
      w: window.innerWidth,
      appid: 'banica',
      status: false,
      user: {},
      imageShare: {},
      id: props.isIndex ? 'index' : props.match.params.id,
      app: { meta: { title: '' } },
    };
  }

  async componentWillMount() {
    await FB.init({
      appId: '329232053871264',
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v2.9',
    });
    const authResponse = await FB.getLoginStatus();
    const me = await FB.me();
    this.setState({
      status: authResponse.status === 'connected',
      user: me,
    });
    this.getContents();
  }

  async Login() {
    const authResponse = await FB.login();
    const me = await FB.me();
    this.setState({
      status: authResponse.status === 'connected',
      user: me,
    });
    this.getContents();
  }

  async getContents() {
    const response = await fetch(`/images/${this.state.id}/items.json`);
    const user = this.state.user;

    const app = await response
      .text()
      .then(text => text.replace('{{first_name}}', user.first_name).replace('{{user_photo}}', user.photo))
      .then(res => JSON.parse(res));
    const rid = Math.floor(Math.random() * app.meta.items + 1);

    const photos = app.main.photos.map((photo) => {
      console.log(photo);
      if (photo.rid) {
        return { ...photo, src: `${photo.src}${rid}.png` };
      }
      return photo;
    });
    app.main.photos = photos;

    const imageshare = app.main.photos.filter(image => image.bg);
    console.log(app);

    this.setState({
      app,
      imageShare: imageshare[0],
    });
  }

  render() {
    return (
      <div>
        <div className="header">{this.state.app.meta.title}</div>
        <div className="radialred">
          {this.props.isIndex ? (
            <div>sss</div>
          ) : this.state.app.main ? (
            <div>
              <Helmet>
                <meta charSet="utf-8" />
                <title>{this.state.app.meta.title}</title>
                <meta
                  property="og:url"
                  content={`https://iztegli.netlify.com/${this.state.id}${
                    this.props.location.search
                  }`}
                />
                <meta property="og:image:width" content={this.state.imageShare.width} />
                <meta property="og:image:height" content={this.state.imageShare.height} />
                <meta property="og:type" content="article" />
                <meta property="og:title" content={this.state.app.meta.title} />
                <meta property="og:description" content={this.state.app.meta.descr} />
                <meta
                  property="og:image"
                  content={`http://lamb2.herokuapp.com/tmp/${this.props.location.search.replace(
                    '?id=',
                    '',
                  )}.png`}
                />
              </Helmet>
              <Canvas data={this.state.app.main} appid={this.state.id} />
            </div>
          ) : (
            <button onClick={() => this.Login()}>Влез с Facebook</button>
          )}
        </div>
        <div className="footer" />
      </div>
    );
  }
}

export default Banica;
