const puppeteer = require('/Volumes/HDD/.npm-global/lib/node_modules/puppeteer/');
const async = require('/Volumes/HDD/.npm-global/lib/node_modules/async/');
const appdir = '/Volumes/HDD/user/Desktop/facebookapps/public/images/quote/';
const items = require(appdir + 'index.json');

const width = 486;
const height = 211;
const fontSize = '170%';

async function procez(text, i, callback) {
  const browser = await puppeteer.launch();

  const page = await browser.newPage();
  await page.setViewport({
    width: width,
    height: height,
  });
  page._emulationManager._client.send(
    'Emulation.setDefaultBackgroundColorOverride',
    { color: { r: 0, g: 0, b: 0, a: 0 } }
  );

  page.setContent(
    "<html><body style=\"padding:0px;margin:0px;\"><div style=\"font-family:'Helvetica Neue'; font-smooth: 'always'; display: flex; font-weight:100; align-items: center; justify-content: center; text-align:'center'; height:" +
      height +
      'px; font-size:' +
      fontSize +
      ';" >' +
      text +
      '</div></body></html>'
  );
  await page.screenshot({ path: appdir + '' + count + '.png' });
  await browser.close();
  callback();
}
let count = 0;
async.eachLimit(
  items.quotes,
  5,
  (i, callback) => {
    procez(i, count, function(params) {
      count++;
      callback();
    });
  },
  err => {
    console.log('Hello');
  }
);
