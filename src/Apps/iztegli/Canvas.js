import React from 'react';

import axios from 'axios';
class Canvas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: 'green',
      img: '/fb/load.gif',
      data: this.props.data,
      appid: this.props.appid,
      dimensions: {},
      textAnchor: {
        center: 'middle',
        right: 'end',
        left: 'start',
      },
      loaded: false,
      numPics: 0,
    };
    this.generateImg = this.generateImg.bind(this);

    //this.onImgLoadUserPic = this.onImgLoadUserPic.bind(this);
  }

  componentWillMount() {
    console.log(this.state.data);

    var loadedImages = [];
    const photos = this.state.data.photos;
    const dimensions = photos.filter(item => (item.bg ? true : false));

    var promiseArray = photos.map(function(imgx) {
      var prom = new Promise(function(resolve, reject) {
        var img = new Image();
        img.crossOrigin = 'anonymous';
        img.src = imgx.src;

        photos.forEach(function(params, i) {
          if (imgx.src === params.src) {
            img.onload = function() {
              loadedImages[i] = img;
              resolve();
            };
          }
        });
      });
      return prom;
    });
    console.log(loadedImages);

    Promise.all(promiseArray).then(() => {
      this.generateImg(loadedImages, dimensions[0].width, dimensions[0].height);
    });
  }

  async generateImg(loadedImages, w, h) {
    const canvas = this.refs.canvas;
    const ctx = canvas.getContext('2d');

    ctx.canvas.width = w;
    ctx.canvas.height = h;

    loadedImages.forEach((item, i) => {
      const { x, y, width, height } = this.state.data.photos[i];

      ctx.drawImage(item, x, y, width, height);
    });
    this.state.data.texts.forEach(item => {
      this.text({ ctx, ...item });
    });

    this.setState({ img: canvas.toDataURL() });
    this.preshare();
  }

  text(props) {
    const { ctx, text, x, y, align, font, size, color } = props;
    ctx.textAlign = align;
    ctx.font = size + 'px ' + font;
    ctx.fillStyle = color;
    ctx.fillText(text, x, y);
  }
  async preshare() {
    const { appid } = this.state;
    const link = await axios
      .post('https://lamb2.herokuapp.com/la/result', {
        image: this.state.img,
      })
      .then(function(response) {
        return (
          'https://iztegli.netlify.com/' + appid + '/?id=' + response.data.id
        );
      });
    console.log(link);
    this.setState({
      link,
    });

    window.FB.api(
      'https://graph.facebook.com/',
      'post',
      {
        id: link,
        scrape: true,
      },
      function(responsex) {}
    );
  }
  share() {
    window.FB.ui(
      {
        method: 'share',
        href: this.state.link,
      },
      function(response) {}
    );
  }
  render() {
    return (
      <div>
        <img className="resultimg" src={this.state.img} alt="" />
        <div
          style={{
            width: 1,
            height: 1,
            overflow: 'hidden',
            position: 'absolute',
            top: -10,
          }}
        >
          <canvas ref="canvas" />
        </div>

        <div className="clear" />
        <button onClick={() => this.share()}>Сподели</button>
      </div>
    );
  }
}
export default Canvas;
