const apps = {
   banica: {
      id: 6,
      bg:
         'https://apps-455676461606571.apps.fbsbx.com/instant-bundle/1983666315024523/1914345808681709/static/images/en/0557/r_06.png',
      photos: [],
      texts: [
         {
            x: 300,
            y: 200,
            size: 45,
            text: '{{me:first_name}},',
            align: 'center',
            color: '#383838',
            font: 'Arial',
            weight: 'bold',
         },
      ],
      context: { text: 'share' },
   },
}
export default apps
