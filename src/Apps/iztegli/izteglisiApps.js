const apps = {
  banica: {
    meta: {
      title: 'Коледна Баница с късмети',
      descr: 'В навечерието на 2019 изтели своя късмет',
      items: 30,
    },
    main: {
      id: 1,
      photos: [
        {
          x: 259,
          y: 45,
          width: 114,
          height: 114,
          src: '{{user_photo}}',
        },
        {
          x: 0,
          y: 0,
          src: '/images/banica/bg.png',
          width: 637,
          height: 387,
          bg: true,
        },
        {
          x: 210,
          y: 195,
          width: 226,
          height: 150,
          src: 'https://iztegli.netlify.com/images/banica/',
          rid: true,
        },
      ],
      texts: [
        {
          x: 316,
          y: 180,
          size: 24,
          text: '{{first_name}}',
          align: 'center',
          color: '#FFF',
          font: 'Arial',
          weight: 'light',
        },
      ],
      context: { text: 'share' },
    },
  },
  quote: {
    meta: {
      title: 'Цитат за деня',
      descr: 'Изтегли късметче с кафето',
      image: '',
      items: 255,
    },
    main: {
      photos: [
        {
          x: 10,
          y: 10,
          width: 102,
          height: 102,
          src: '{{user_photo}}',
        },
        {
          x: 0,
          y: 0,
          src: '/images/quote/bg.png',
          width: 700,
          height: 467,
          bg: true,
        },
        {
          x: 111,
          y: 145,
          width: 487,
          height: 212,
          src: '/images/quote/',
          rid: true,
        },
      ],
      texts: [
        {
          x: 180,
          y: 70,
          size: 24,
          text: '{{first_name}}',
          align: 'center',
          color: '#333222',
          font: 'Arial',
          weight: 'light',
        },
      ],
      context: { text: 'share' },
    },
  },
};
