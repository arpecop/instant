/* eslint-disable react/prop-types */
import React from 'react';
import { Helmet } from 'react-helmet';
import db from '../../api';
import Item from './item';
import './src/main.css';
import { apiParams, cats, filterImages, query } from './src/settings';
import { Foot, Head, Promiss, Loading } from './UI';

const Meta = props => {
     const { title, screenName } = props;

     return (
          <Helmet>
               <title>{`${screenName}: ${title}`}</title>
               <meta property="og:url" content={window.location.href} />
               <meta property="og:type" content="article" />
               <meta property="og:title" content={title} />
          </Helmet>
     );
};

const Feed = ({ usePaginator, extra, view, title, picOnly, useMeta }) => {
     const { data, isLoading } = db.get(
          query({
               view,
               include_docs: true,
               descending: 'true',
               ...(extra || {}),
          })
     );
     if (isLoading) return <Loading />;
     if (data && data.rows[0]) {
          const isPaging = data.rows.length >= apiParams.limit;
          const arr = picOnly ? filterImages(data.rows, 20) : data.rows;

          return (
               <>
                    {useMeta ? <Meta {...data.rows[0]} /> : null}
                    <h1 style={{ fontFamily: 'Roboto', marginLeft: 165 }}>{title}</h1>
                    {usePaginator && isPaging ? (
                         <Promiss.Paginator view={view} id={extra.start_key || extra.key} usePaginator={usePaginator} />
                    ) : null}
                    {arr.map(item => {
                         return <Item item={item} key={item._id} picOnly />;
                    })}
               </>
          );
     }
     return null;
};

// MAIN

class Twitter extends React.Component {
     state = {
          type: null,
          id: null,
          cat: {
               id: 'Home',
               url: '/',
               icon: 'home',
               tags: 'viral,trend,retweet',
          },
     };

     async componentWillMount() {
          const { match } = this.props;
          // const { cat } = this.state;
          const type = match ? match.params.id : 'home';
          const id = match ? match.params.id2 : null;
          const page = match ? match.params.start_key : null;

          this.setState(prevState => {
               return {
                    type,
                    id,
                    page,
                    cat: id ? cats[id] : prevState.cat,
               };
          });
     }

     render() {
          const { id, type, page, cat } = this.state;

          return (
               <div style={{ paddingTop: 60, paddingLeft: 5, paddingRight: 85 }}>
                    {type === 'home' ? (
                         <>
                              <Promiss.Home />

                              <Foot />
                         </>
                    ) : null}

                    {type === 'en' ? (
                         <>
                              <Head title={`/${cat.id}`} active={cat.id} icon={cat.icon} />
                              <Feed
                                   view="tags"
                                   extra={{
                                        keys: cat.tags,
                                        reduce: false,
                                        group: false,
                                        limit: 50,
                                   }}
                              />
                              <Foot />
                         </>
                    ) : null}
                    {type === 'u' ? (
                         <>
                              <Head title={`@${id}`} />
                              <Feed
                                   view="users"
                                   extra={{
                                        key: `"${id}"`,
                                        reduce: false,
                                   }}
                                   useMeta
                              />
                              <Promiss.TagList
                                   view="users"
                                   extra={{
                                        start_key: `"${id}"`,
                                        skip: 10,
                                   }}
                                   title="user"
                                   preUrl="/u/"
                              />
                              <Promiss.TagList
                                   title="tags"
                                   view="tags"
                                   extra={{
                                        start_key: `"${id}"`,
                                   }}
                                   preUrl="/t/"
                              />
                         </>
                    ) : null}
                    {type === 't' ? (
                         <>
                              <Head title={`#${id}`} />
                              <Feed
                                   view="tags"
                                   extra={{
                                        key: `"${id}"`,
                                        reduce: false,
                                        skip: page ? page * apiParams.limit - apiParams.limit : 0,
                                   }}
                                   usePaginator={{ href: '/t', page }}
                                   useMeta
                              />
                              {!page ? (
                                   <Promiss.TagList
                                        view="tags"
                                        title=""
                                        extra={{
                                             start_key: `"${id}"`,
                                             limit: 50,
                                        }}
                                        preUrl="/t/"
                                   />
                              ) : null}
                         </>
                    ) : null}
               </div>
          );
     }
}

export default Twitter;
