//import { useGlobal } from 'reactn';
import { setGlobal, useGlobal, addReducer } from 'reactn';

const handleResize = () => {
  setGlobal({
    h: window.innerHeight,
    w: window.innerWidth,
  });
};

handleResize();

window.addEventListener('resize', handleResize);
