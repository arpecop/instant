import PropTypes from "prop-types";
import React, { useState } from "react";
import {
  Divider,
  Icon,
  Layout,
  List,
  Menu,
  Row,
  Tag,
  Badge,
  Button,
  Col,
  Card,
  Spin
} from "antd";
import db from "../../api";
import { apiParams, cats, headerMenu, query } from "./src/settings";

function Wrapper({ children }) {
  return (
    <Row type="flex" justify="center" style={{ marginBottom: 70 }}>
      <Col xs={24} sm={21} md={18} lg={11}>
        {children}
      </Col>
    </Row>
  );
}

function Head({ title, active, icon }) {
  const [collapsed, setCollapsed] = useState(true);
  return (
    <>
      <div
        style={{
          position: "fixed",
          padding: 15,
          right: collapsed ? 80 : 190,
          top: 0,
          zIndex: 100
        }}
      >
        <Icon
          style={{ fontSize: 22 }}
          className="trigger"
          type={collapsed ? "menu-fold" : "menu-unfold"}
          onClick={() => setCollapsed(collapsed !== true)}
        />
      </div>
      <Layout.Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        style={{
          position: "fixed",
          top: 0,
          right: 0,
          zIndex: 100
        }}
      >
        <Menu theme="dark" defaultSelectedKeys={[active]}>
          <Menu.Item key="Home">
            <a href="/">
              <Icon type="home" />
              <span>Home</span>
            </a>
          </Menu.Item>
          {headerMenu.map(item =>
            item.subitems ? (
              <Menu.SubMenu
                key={`${item.id}x`}
                title={
                  <span>
                    <Icon type={item.icon} />
                    <span>{item.id}</span>
                  </span>
                }
              >
                {item.subitems.map(itemx => (
                  <Menu.Item key={itemx.id}>
                    <a href={`/en/${itemx.id}`}>
                      {itemx.icon ? <Icon type={itemx.icon} /> : null}
                      <span>{itemx.id}</span>
                    </a>
                  </Menu.Item>
                ))}
              </Menu.SubMenu>
            ) : (
              <Menu.Item key={item.id}>
                <a href={`/en/${item.id}`}>
                  {item.icon ? <Icon type={item.icon} /> : null}
                  <span>{item.id}</span>
                </a>
              </Menu.Item>
            )
          )}
        </Menu>
      </Layout.Sider>
      <div
        style={{
          height: 72,
          textAlign: "center",
          marginBottom: 4,
          top: -10,
          left: 0,
          position: "absolute",
          transform: "rotate(-2.5deg)",
          zIndex: 102
        }}
      >
        <div
          className="logo"
          style={{
            backgroundColor: "#95a5a6",
            color: "#262a31",
            float: "left",
            paddingLeft: 10,
            paddingRight: 10,
            height: 64,
            lineHeight: "84px",
            fontSize: "1.6rem"
          }}
        >
          <div
            style={{
              fontFamily: "Roboto",
              padding: 0,
              margin: 0,
              verticalAlign: "middle"
            }}
          >
            <Icon
              type={icon || "deployment-unit"}
              style={{ fontSize: "2.3rem" }}
            />
            {`   ${title}`}
          </div>
        </div>
      </div>
    </>
  );
}

function TagList({ arr, title, preUrl }) {
  return (
    <Wrapper>
      {title ? (
        <Divider orientation="left" style={{ color: "#cfc3c2" }}>
          <Tag color="#95a5a6">
            <Icon type={title} />
            {`${title}`}
          </Tag>
        </Divider>
      ) : null}

      <Row style={{ textAlign: "center" }}>
        <List
          itemLayout="horizontal"
          dataSource={arr}
          renderItem={item => (
            <a href={`${preUrl}${item.key}`}>
              <Badge
                count={item.value > 1 ? item.value : 0}
                style={{
                  backgroundColor: "#95a5a6",
                  border: "none",
                  boxShadow: "none"
                }}
              >
                <Tag style={{ color: "#252425" }}>{item.key}</Tag>
              </Badge>
            </a>
          )}
        />
      </Row>
    </Wrapper>
  );
}
function Foot() {
  return (
    <Row
      style={{
        padding: 10,
        fontFamily: "Roboto",
        fontSize: "0.7rem",
        clear: "both"
      }}
    >
      <h2>What is tIncubatar?</h2>
      <p>tIncubtar aggregates top news and information in real time. </p>
      <p style={{ textAlign: "right" }}>
        Ⓒ 2019 tIncubtar | Twitter Web Viewer uses the Twitter API but is not
        endorsed or certified by Twitter.
      </p>
    </Row>
  );
}

const Promiss = {
  Widget: ({ url, oc }) => {
    const { data } = db.getUrl(url);
    if (data) {
      return (
        <Card.Meta
          title={data.title}
          description={
            data.description ? (
              <div style={{ marginBottom: 10 }}>
                <img src={data.image} alt="" style={{ maxWidth: "100%" }} />
                {data.description}
              </div>
            ) : (
              <div style={{ marginBottom: 10 }}>{oc}</div>
            )
          }
        />
      );
    }
    return <div style={{ marginBottom: 10 }}>{oc}</div>;
  },
  Home: () => {
    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }
    const catsAsKeys = Object.keys(cats).map(i => i.toLowerCase());
    const { data, isLoading } = db.get(
      query({
        reduce: false,
        group: false,
        limit: 5,
        view: "tags",
        include_docs: true,
        fields: "title,_id,query,screenName",
        keys: catsAsKeys.join(",")
      })
    );
    if (isLoading) {
      return <Loading />;
    }
    if (data) {
      const x = Object.keys(cats).reduce((acc, cur) => {
        acc[cur] = {};
        acc[cur].id = cur;
        acc[cur].items = data.rows.filter(
          item => capitalizeFirstLetter(item.query) === cur
        );
        return acc;
      }, {});

      return (
        <Layout.Content>
          <Head title="tIncubator" active="Home" />
          <div
            style={{
              fontFamily: "Roboto",
              maxWidth: "20%",
              textAlign: "right"
            }}
          >
            <h2>Categories</h2>
          </div>
          {Object.values(x).map(cat => (
            <div key={cat.id}>
              <a
                href={`/en/${cat.id}`}
                style={{
                  textAlign: "right",
                  backgroundColor: "transparent",
                  overflow: "hidden",
                  width: "20%",
                  fontSize: "1.3rem",
                  textOverflow: "ellipsis",
                  float: "left"
                }}
              >
                {cat.id}
              </a>
              {cat.items.map(item => (
                <a
                  key={`${item._id}${item.screenName}`}
                  href={`/u/${item.screenName}`}
                  style={{
                    textAlign: "left",
                    backgroundColor: "transparent",
                    overflow: "hidden",
                    width: "80%",
                    float: "right",
                    paddingTop: 5,
                    paddingLeft: 20
                  }}
                >
                  {item.title}
                  <hr />
                </a>
              ))}
            </div>
          ))}
        </Layout.Content>
      );
    }
    return null;
  },
  TagList: ({ view, extra, title, preUrl }) => {
    const { data } = db.get(
      query({
        view,
        reduce: true,
        group: true,
        ...extra
      })
    );

    if (data) {
      return <TagList title={title} preUrl={preUrl} arr={data.rows} />;
    }
    return null;
  },
  Paginator: ({ id, view, usePaginator }) => {
    const { page, href } = usePaginator;
    const { data } = db.get(
      query({
        reduce: true,
        group: true,
        view,
        key: id
      })
    );

    if (data) {
      const genArray = (numPages, itemsPerPage, currentPage) => {
        const arr = [...Array(Math.round(numPages / itemsPerPage)).keys()].map(
          i => i + 1
        );
        const pFive = currentPage + 5;
        const mFive = currentPage - 5;
        const getFirstAnchor = mFive <= 1 ? 0 : mFive;
        const getLastAnchor =
          getFirstAnchor + pFive <= 10 ? Math.abs(mFive) + pFive : pFive;
        return arr.slice(getFirstAnchor, getLastAnchor);
      };

      const arrItems = genArray(
        data.rows[0].value,
        apiParams.limit,
        Math.round(page || 1)
      );

      return (
        <div>
          <div
            style={{
              textAlign: "center",
              position: "fixed",
              bottom: 0,
              zIndex: 100,
              width: "100%",
              height: 50,
              paddingTop: 9,
              backgroundColor: "#2d3436"
            }}
          >
            <Button.Group size="default">
              {arrItems.map(p => (
                <Button
                  key={p}
                  type="primary"
                  href={`${href}/${data.rows[0].key}${p > 1 ? `/${p}` : ""}`}
                  ghost
                >
                  {p === Math.round(page) ? <Badge status="default" /> : p}
                </Button>
              ))}
            </Button.Group>
          </div>
        </div>
      );
    }
    return null;
  }
};
const Loading = () => {
  return (
    <div
      style={{
        position: "fixed",
        width: "100%",
        height: "100%",
        top: 0,
        bottom: 0,
        textAlign: "center",
        paddingTop: "19%",
        backgroundColor: "#262a31",
        zIndex: 101
      }}
    >
      <Spin size="large" />
    </div>
  );
};

Head.propTypes = {
  title: PropTypes.string.isRequired,
  active: PropTypes.string,
  icon: PropTypes.string
};
Wrapper.propTypes = {
  children: PropTypes.node.isRequired
};

TagList.propTypes = {
  arr: PropTypes.array.isRequired,
  preUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export { TagList, Wrapper, Head, Foot, Promiss, Loading };

// export default TagList;
