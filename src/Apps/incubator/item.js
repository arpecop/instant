import { Avatar, Button, Card, Col, Comment, Icon, Row, Tag } from "antd";
import isbot from "isbot";
import shuffle from "lodash/shuffle";
import PropTypes from "prop-types";
import React from "react";
import LazyLoad from "react-lazy-load";
import { Promiss } from "./UI";

const uuid = require("uuid/v4");

function TextFormat({ text }) {
  const arr = text.split(" ").map(word => {
    if (word.substring(0, 1) === "#") {
      return { type: "tag", word, key: uuid() };
    }
    if (word.substring(0, 3) === "http") {
      return { type: "url", word: "xxx", key: uuid() };
    }
    return { word, type: "word", key: uuid() };
  });
  const cont = arr.map(word => {
    if (word.type === "url") {
      return <React.Fragment key={word.key}>dsdsxxxxx</React.Fragment>;
    }
    if (word.type === "tag") {
      return (
        <a
          key={word.key}
          href={`/t/${word.word
            .toLowerCase()
            .replace("#", "")
            .replace(".", "")}`}
        >
          <Tag style={{ color: "#161616", backgroundColor: "#95a5a6" }}>
            {word.word}
          </Tag>
        </a>
      );
    }
    return <React.Fragment key={word.key}>{`${word.word} `}</React.Fragment>;
  });

  return (
    <div
      style={{
        textOverflow: "ellipsis"
      }}
    >
      {cont}
    </div>
  );
}

function Komentar({ children, item }) {
  let title = item.title || item.text;
  const url = item.urls && item.urls[0] ? item.urls[0] : null;

  title = url ? title.replace(url, "") : title;

  return (
    // eslint-disable-next-line react/jsx-filename-extension
    <div>
      <div
        style={{
          minWidth: 100,
          position: "absolute",
          right: 0,
          fontSize: 14
        }}
      />
      <Comment
        // actions={[<span>Reply to</span>]}
        datetime={new Date(Math.round(item.date)).toDateString()}
        author={
          <a style={{ color: "#FFF" }} href={`/u/${item.screenName}`}>
            {item.screenName}
          </a>
        }
        avatar={
          <LazyLoad offset={200}>
            <Avatar
              src={`https://twitter.com/${item.screenName}/profile_image?size=normal`}
              size="large"
            />
          </LazyLoad>
        }
        content={
          isbot(navigator.userAgent) ? (
            <TextFormat text={shuffle(title.split(" ")).join(" ")} />
          ) : (
            <TextFormat text={title} />
          )
        }
      >
        {children}
      </Comment>
    </div>
  );
}

function Item({ item }) {
  const { screenName, quote, images, urls, title, meta } = item;

  const href = `/u/${screenName}`;
  const thread = quote ? (
    <Komentar item={quote} href={href}>
      <Komentar item={item} href={href} />
    </Komentar>
  ) : (
    <Komentar item={item} href={href} />
  );
  return (
    <Row type="flex" justify="center">
      <Col xs={24} sm={20} md={18} lg={10}>
        <Card
          bordered={false}
          type="inner"
          style={{
            marginBottom: 25,
            backgroundColor: "#161616",
            color: "#b2bec3",
            overflow: "hidden",
            borderColor: "#252425"
          }}
          cover={
            images ? (
              <LazyLoad offset={200}>
                <a href={href}>
                  <img alt="" style={{ width: "100%" }} src={images[0]} />
                </a>
              </LazyLoad>
            ) : null
          }
        >
          {urls && urls[0] && !meta ? (
            <div>
              <LazyLoad offset={200}>
                <Promiss.Widget
                  url={`https://lambdata.herokuapp.com/la/meta?url=${urls[0].url}`}
                  oc={title.replace(urls[0].url, "")}
                />
              </LazyLoad>
              <Button
                rel="nofollow"
                href={urls[0].url}
                block
                style={{ backgroundColor: "#1a191a", color: "#FFF" }}
              >
                <Icon type="compass" theme="twoTone" />
                {` ${urls[0].url.split("/")[2]}`}
              </Button>
            </div>
          ) : (
            thread
          )}
        </Card>
      </Col>
    </Row>
  );
}
TextFormat.propTypes = {
  text: PropTypes.string.isRequired
};
Komentar.propTypes = {
  children: PropTypes.node,
  item: PropTypes.object.isRequired
};
Item.propTypes = {
  item: PropTypes.object.isRequired
};

export default Item;
