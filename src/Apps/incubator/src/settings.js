export const apiParams = {
  limit: 10,
  design: 'api',
  descending: true,
  db: 'twitter',
  update: false,
};

export const headerMenu = [
  {
    id: 'Viral',
    icon: 'fire',
    tags: 'viral,trend,retweet',
  },
  {
    id: 'News',
    icon: 'global',
    subitems: [
      { id: 'News', tags: 'news,breakingnews,today,cnn,cbs,foxnews' },
      { id: 'Politics', tags: 'polytics,trump' },
    ],
  },
  {
    id: 'Tech',
    icon: 'laptop',
    subitems: [
      { id: 'Apple', tags: 'apple,ios,macos,iphone', icon: 'apple' },
      {
        id: 'Android',
        tags: 'android,samsung,huawei,xiaomi,xperia,googleplay',
        icon: 'android',
      },
      {
        id: 'Linux',
        tags: 'linux,ubuntu,kernel,redhat,fedora,opensource',
        icon: 'qq',
      },
    ],
  },
  {
    id: 'Sport',

    icon: 'trophy',
    tags: 'sport,football,nba,hockey,tennis,baseball,ufc',
  },
  {
    id: 'Entertainment',

    icon: 'rest',
    subitems: [
      { id: 'Celebrities', tags: 'mtv,celeb,bieber,beyonce,kardashian' },
      { id: 'Funny', tags: 'meme,memes,kek,funny,cringe,lmao,lmfao,dankmemes' },
      { id: 'Video', tags: 'video,musicvideo,youtube' },
      { id: 'Movies', tags: 'movie,imdb,oscar' },
      { id: 'Television', tags: 'tv,show' },
      { id: 'Gaming', tags: 'game,gaming' },
    ],
  },
  {
    id: 'Lifestyle',

    icon: 'skin',
    subitems: [
      { id: 'Health', tags: 'health,healthy' },
      { id: 'Fitness', tags: 'fitness,run,jogging' },
      { id: 'Travel', tags: 'travel' },
      { id: 'Beauty', tags: 'beauty' },
      { id: 'Food', tags: 'food' },
      { id: 'Photography', tags: 'photography' },
      { id: 'Deals', tags: 'deal,coupon,voucher' },
      { id: 'Lifehack', tags: 'dyi,lifehack' },
      { id: 'Astrology', tags: 'zodiac,astrology' },
    ],
  },
  {
    id: 'Finance',
    icon: 'stock',
    subitems: [
      { id: 'Business', tags: 'business,stock,stocks,dowjones' },
      { id: 'Crypto', tags: 'crypto,bitcoin,blockchain' },
      { id: 'Seo', tags: 'serp,seo,google' },
    ],
  },
];
// console.log(JSON.stringify(headerMenu, 0, 4));

export const cats = headerMenu
  .flatMap(f => (f.subitems ? f.subitems : f))
  .reduce((acc, cur) => {
    acc[cur.id] = cur;
    return acc;
  }, {});

export const filterImages = (arr, limit) => arr.filter(i => i.images[0]).slice(0, limit);

export const query = q => ({
  ...apiParams,
  ...q,
});
