class Complete extends React.Component {
  state = {
    result: [{ key: "Loading" }],
    redirect: null
  };
  query(view, term) {
    return {
      view,
      limit: 2,
      group: true,
      reduce: true,
      start_key: '"' + term + '"',
      db: "twitter",
      include_docs: false
    };
  }

  handleSearch = async value => {
    let sTags = this.query("tags", value);
    //let sUsers = this.query('users', value);
    this.setState({ result: [{ key: "Loading" }] });
    const responseTags = await fetch(db.toUrl(sTags));
    //const responseUsers = await fetch(db.toUrl(sUsers));
    const tags = await responseTags.json();
    //const users = await responseUsers.json();
    const combined = shuffle([...tags.rows.map(i => ({ ...i, icon: "tag" }))]);
    this.setState({ result: combined });
  };
  select(redirect) {
    this.setState({ redirect });
  }
  render() {
    const { result, redirect } = this.state;
    const children = result.map(result => (
      <AutoComplete.Option key={result.key} title={result.key}>
        {result.key === "Loading" ? (
          <Spin />
        ) : (
          <span>
            <Icon
              type={result.icon}
              twoToneColor="#eb2f96"
              style={{ size: 42 }}
            />
            {result.key}
          </span>
        )}
      </AutoComplete.Option>
    ));
    return (
      <div>
        <AutoComplete
          onSelect={this.select.bind(this)}
          onSearch={this.handleSearch}
          placeholder="search users or tags"
          style={{ width: "100%" }}
        >
          {children}
        </AutoComplete>

        {redirect ? <Redirect to={"/t/" + redirect} /> : null}
      </div>
    );
  }
}
