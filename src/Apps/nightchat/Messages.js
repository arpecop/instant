import React from 'react';
import { Comment, Avatar } from 'antd';
import { emojify } from 'react-emojione';
import autoscroll from 'autoscroll-react';

const Message = props => {
  const { messageText, nickname, from } = props.item;

  return (
    <Comment
      author={nickname}
      avatar={
        <Avatar
          src={`https://graph.facebook.com/${from}/picture?height=50&type=normal&width=50`}
          alt=""
        />
      }
      content={<p>{messageText.length >= 2 ? emojify(messageText) : ''}</p>}
    />
  );
};
class Messages extends React.PureComponent {
  render() {
    const { items, height } = this.props;

    return (
      <ul
        style={{
          overflowY: 'scroll',
          height: height - 111
        }}
      >
        {items.map(item => (
          <Message key={item.id} item={item.value ? item.value : item} />
        ))}
      </ul>
    );
  }
}

export default autoscroll(Messages, { isScrolledDownThreshold: 100 });
