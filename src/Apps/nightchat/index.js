/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Tabs, Icon, Badge, Input, Modal } from 'antd';
import windowSize from 'react-window-size';
import slugify from 'react-slugify';
import Cookies from 'universal-cookie';
import ReconnectingWebSocket from 'reconnecting-websocket';
import { Facebook } from '../../Facebook';
import '../incubator/src/main.css';
import Messages from './Messages';
import { getApi, putApi, ws } from './api';

const cookies = new Cookies();
const FB = new Facebook();

class NightChat extends Component {
  constructor(props) {
    super();
    this.state = {
      status: false,
      user: {},
      value: '',
      messages: [],
      channel: props.isIndex ? 'nightchat' : props.match.params.id,
      nickname: null,
      isIndex: props.isIndex,
      settings: props.isIndex
        ? { comment: 'Напиши съобщение' }
        : { comment: 'Добави Коментар' }
    };
  }

  async componentWillMount() {
    console.log(this.state.channel);

    await FB.init({
      appId: '181361935494',
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v2.9'
    });
    const authResponse = await FB.getLoginStatus();
    const me = await FB.me();
    const history = await getApi({
      db: 'nightchat',
      view: 'messages',
      descending: true,
      key: `"${this.state.channel}"`,
      limit: 100
    });
    const nickname = cookies.get('nickname');

    this.setState({
      status: authResponse.status === 'connected',
      user: me,
      nickname: nickname || 'Анонимен',
      messages: history.rows.reverse()
    });
    console.log(this.state.channel);
    const rws = new ReconnectingWebSocket(
      `wss://rudixauth.herokuapp.com/${this.state.channel}`
    );
    rws.addEventListener('message', message => {
      const transaction = JSON.parse(message.data).json;
      const { messages, user } = this.state;
      if (transaction.from !== user.id) {
        this.setState({
          value: '',
          messages: [...messages, transaction]
        });
      }
    });
  }

  _handleKeyDown = e => {
    const { channel, user, value, messages, nickname } = this.state;
    if (e.key === 'Enter' && value.length >= 2) {
      const transaction = {
        id: new Date().getTime().toString(),
        from: user.id,
        to: `${channel}`,
        channel: `${channel}`,
        typex: 'message',
        messageText: value,
        nickname: nickname || 'Анонимен'
      };
      console.log(transaction);

      putApi(transaction);
      ws(transaction);
      this.setState({
        value: '',
        messages: [...messages, transaction]
      });
    }
  };

  async Login() {
    const authResponse = await FB.login();
    const me = await FB.me();
    this.setState({
      status: authResponse.status === 'connected',
      user: me
    });
  }

  changeNickname(nickname1) {
    const nickname = slugify(nickname1);
    cookies.set('nickname', nickname, { path: '/' });
    Modal.success({
      title: nickname,
      content: 'прякора ви е сменен'
    });
    this.setState({
      nickname
    });
  }

  render() {
    const { status, value, messages, nickname, settings } = this.state;
    const { windowHeight } = this.props;
    return (
      <Tabs defaultActiveKey="1" style={{ textAlign: 'left' }}>
        <Tabs.TabPane
          tab={
            <div style={{ width: 120 }}>
              <div
                style={{
                  fontWeight: 'lighter',
                  marginTop: -20,
                  position: 'absolute',
                  fontSize: 27
                }}
              >
                #NightChat
              </div>
            </div>
          }
          key="1"
        >
          {status ? (
            <div>
              <div className="messages">
                <Messages items={messages} height={windowHeight} />
              </div>
              <input
                onKeyDown={this._handleKeyDown}
                placeholder={settings.comment}
                value={value}
                onChange={evt => {
                  this.setState({ value: evt.target.value });
                }}
                style={{
                  fontWeight: 'lighter',
                  backgroundColor: 'transparent',
                  border: 'none',
                  position: 'fixed',
                  bottom: 0,
                  width: '100%',
                  height: 53,
                  paddingLeft: 5,
                  left: 0,
                  color: '#FFF',
                  borderTop: '2px solid #6c5ce7'
                }}
              />
            </div>
          ) : (
            <div
              style={{
                position: 'fixed',
                width: '100%',
                height: '100%',
                top: 0,
                zIndex: 101,
                backgroundColor: '#262a31'
              }}
            >
              <h1
                style={{
                  fontWeight: 'lighter',
                  fontSize: 27
                }}
              >
                #NightChat
              </h1>
              <div
                style={{
                  textAlign: 'center',
                  marginTop: windowHeight / 2 - 120
                }}
              >
                <button
                  type="button"
                  style={{
                    color: '#FFF',
                    backgroundColor: 'transparent',
                    margin: 'auto',
                    cursor: 'pointer',
                    padding: 10,
                    borderRadius: 5,
                    border: '2px solid #6055a0'
                  }}
                  onClick={() => this.Login()}
                >
                  <img
                    src={require('./rsrc/fb.png')}
                    alt=""
                    style={{
                      position: 'absolute',
                      marginTop: -13,
                      maxWidth: 50,
                      marginLeft: -47
                    }}
                  />
                  Влез с Facebook
                </button>
              </div>
            </div>
          )}
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <div style={{ textAlign: 'center' }}>
              <Icon type="idcard" theme="filled" style={{ fontSize: '2rem' }} />
            </div>
          }
          key="3"
        >
          <div
            style={{ textAlign: 'center', marginTop: windowHeight / 2 - 110 }}
          >
            <Input.Search
              style={{
                width: '80%',
                margin: 'auto'
              }}
              placeholder="Смени прякора си"
              onChange={evt => {
                this.setState({ nickname: evt.target.value.substring(0, 14) });
              }}
              value={nickname}
              onSearch={value => this.changeNickname(value)}
              enterButton={<Icon type="save" theme="filled" />}
            />
          </div>
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <div style={{ textAlign: 'center' }}>
              <Badge count={1}>
                <Icon
                  type="message"
                  theme="filled"
                  style={{ fontSize: '2rem' }}
                />
              </Badge>
            </div>
          }
          key="2"
        >
          <div
            style={{
              position: 'fixed',
              width: '100%',
              height: '100%',
              backgroundColor: '#4f5259'
            }}
          >
            <a
              href="https://play.google.com/store/apps/details?id=com.rudixlabs.nightchat2"
              target="_top"
              alt="nightchat"
            >
              <img
                src={require('./rsrc/nightchatBanner.jpg')}
                alt=""
                style={{ maxWidth: '100%', margin: 'auto' }}
              />
            </a>
          </div>
        </Tabs.TabPane>
      </Tabs>
    );
  }
}
export default windowSize(NightChat);
