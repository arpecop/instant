// import dbx from 'react-native-simple-store';

// http://192.168.0.2:3000/jokes/
const url = 'https://couched.herokuapp.com/nightchat/';

const apiurl = 'https://rudixauth.herokuapp.com/test/api/';

const toUrl = json => {
  const checkForKey = json.key ? { key: `"${json.key}"` } : {};
  const keys = json.keys ? `/${json.keys}` : '';
  const checkForStartKey = json.start_key
    ? { start_key: `"${json.start_key}"` }
    : {};
  const params = Object.keys({ ...json, ...checkForKey, ...checkForStartKey })
    .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(json[k])}`)
    .join('&');
  return `${apiurl}${json.db}/${json.view}${keys}?${params}`;
};
export const getApi = async json => {
  return new Promise(resolve => {
    fetch(toUrl(json))
      .then(response => response.json())
      .then(jsonx => {
        resolve(jsonx);
      })
      .catch(error => {
        console.log(error);

        resolve(error);
      });
  });
};
export const putApi = async json => {
  await fetch(
    `${url}${json._id ? json._id : new Date().getTime().toString()}`,
    {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(json)
    }
  )
    .then(response => response.json())
    .then(jsonx => {
      return new Promise(resolve => {
        resolve(jsonx);
      });
    });
};

export const ws = async json => {
  await fetch(`https://rudixauth.herokuapp.com/sendtochannel/${json.channel}`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ json })
  })
    .then(response => response.json())
    .then(jsonx => {
      return new Promise(resolve => {
        resolve(jsonx);
      });
    });
};

export const getId = async id =>
  new Promise(resolve => {
    fetch(`${url}${id}`)
      .then(response => response.json())
      .then(json => {
        resolve(json);
      })
      .catch(error => {
        resolve(error);
      });
  });
