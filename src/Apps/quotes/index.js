import React, { Component } from 'react';
import { Card, Col, Row } from 'antd';
import windowSize from 'react-window-size';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import axios from 'axios';

const getApi = async url => {
     return new Promise(resolve => {
          axios.get(`https://pouchdb.herokuapp.com/chetiva/${url}`).then(res => {
               resolve(res.data);
          });
     });
};

const Refine = item => {
     if (item.item && item.item.tag === 'p' && item.item.child) {
          return <p>{item.item.child[0].text}</p>;
     }
     if (item.item && item.item.tag === 'img') {
          return (
               <div style={{ textAlign: 'center' }}>
                    <LazyLoadImage
                         alt="example"
                         src={item.item.attr.src}
                         style={{ maxWidth: '100%', margin: 'auto' }}
                    />
               </div>
          );
     }
     if (item.item.text) {
          return null;
     }
     return null;
};

class NightChat extends Component {
     constructor(props) {
          super();
          this.state = {
               isIndex: props.isIndex,
               singles: [],
               single: null,
          };
     }

     async componentWillMount() {
          const { isIndex } = this.state;
          const { match } = this.props;
          const arts = await getApi('_design/i/_view/News?limit=20&descending=true');

          const single = await getApi(isIndex ? '' : match.params.id);

          this.setState({
               singles: arts.rows,
               single,
               // single
          });
     }

     render() {
          const { isIndex, singles, single } = this.state;

          return (
               <>
                    {isIndex || !single ? null : (
                         <>
                              <Row type="flex" justify="center">
                                   <Col xs={22} sm={22} md={20} lg={19} xl={15}>
                                        <h3 style={{ fontWeight: 100 }}>{single.title}</h3>
                                        <Card
                                             hoverable
                                             style={{ border: 'none' }}
                                             cover={
                                                  single.image ? (
                                                       <div style={{ textAlign: 'center' }}>
                                                            <LazyLoadImage alt="example" src={single.image} />
                                                       </div>
                                                  ) : null
                                             }
                                        />
                                        {single.react.child.map(item => (
                                             <Refine item={item} />
                                        ))}
                                        <a href={single.href} rel="nofollow">
                                             {`  източник:  ${single.source} »`}
                                        </a>
                                   </Col>
                              </Row>
                         </>
                    )}

                    <Row type="flex" justify="center">
                         {singles.map(item => (
                              <Col xs={22} sm={20} md={15} lg={11} xl={5} key={item.key}>
                                   <a href={`http://novinata.netlify.com/${item.key}`}>
                                        <Card
                                             hoverable
                                             style={{ border: 'none' }}
                                             cover={
                                                  item.value.image ? (
                                                       <div style={{ textAlign: 'center' }}>
                                                            <LazyLoadImage
                                                                 alt="example"
                                                                 src={item.value.image}
                                                                 style={{ width: '100%' }}
                                                            />
                                                       </div>
                                                  ) : null
                                             }
                                        >
                                             <Card.Meta title={item.value.title} />
                                        </Card>
                                   </a>
                              </Col>
                         ))}
                    </Row>
               </>
          );
     }
}
export default windowSize(NightChat);
