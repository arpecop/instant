import React, { Component } from 'react';

import db from '../../api';
import PatternLock from 'react-pattern-lock';
import shuffle from 'lodash/shuffle';
import localeStrings from '../../locale';

const Points = props => {
  return (
    <div className="points">
      <div className="one">
        <img alt="" src="img/coins/Untitled-1.png" />
      </div>
      <div className="two">{props.score}</div>
      <div className="three">
        <img alt="" src="img/coins/Untitled-3.png" />
      </div>
    </div>
  );
};

class Level extends Component {
  constructor(props) {
    super();
    this.state = {
      letters: [],
      all: [],
      locale: props.lang,
      h: window.innerHeight,
      w: window.innerWidth,
      currentWord: [],
      dots: [],
      level: {},
      levels: [],
      score: { num: 0 },
      lastLevel: { num: 0 },
      levelFinished: true,
    };
  }

  async componentWillMount() {
    const response = await fetch('./json/igradumi.json');
    const json = await response.json();
    const levels = json[this.state.locale];
    const lastLevel = await db.get('lastLevel');
    const level = levels[lastLevel ? lastLevel.num : 0];
    const state = await this.buildDots(level);
    this.setState({ ...state, levels });
    console.log(window.user);
  }

  startLevel() {
    this.setState({ levelFinished: false });
  }

  //build dots

  async buildDots(level) {
    const points = await db.get('score');
    const lastLevel = await db.get('lastLevel');
    const letters = level.letters.split('');
    const dots = [];
    letters.map(item => dots.push(...shuffle(level.letters)));

    return new Promise(resolve => {
      resolve({
        dots,
        letters,
        level,
        lastLevel: lastLevel ? lastLevel : { num: 0 },
        score: points ? points : { num: 0 },
        all: level.allpossible,
        levelFinished: true,
      });
    });
  }
  // check pattern and add points also add level
  async onDotConnect(pattern) {
    var joined = this.state.currentWord.concat({
      key: pattern,
      letter: this.state.dots[pattern],
    });
    this.setState({ currentWord: joined });
    console.log();
  }
  async checkPattern(pattern) {
    const word = pattern.map(doti => this.state.dots[doti]).join('');
    const check = this.state.all.filter(wordx =>
      word === wordx ? word : null
    );
    const score = check[0]
      ? pattern.reduce((a, b) => a + b, 0) +
        pattern.length * 10 +
        this.state.score.num
      : this.state.score.num;
    const lastLevel = check[0]
      ? this.state.lastLevel.num + 1
      : this.state.lastLevel.num;

    await db.put({ _id: 'score', num: score });
    await db.put({ _id: 'lastLevel', num: lastLevel });

    const test = await this.buildDots(this.state.levels[lastLevel]);
    const stat = check[0] ? { ...test } : {};

    return new Promise((resolve, reject) => {
      this.setState({
        ...stat,
        currentWord: [],
        score: {
          num: score,
        },
      });
      setTimeout(() => (check[0] ? resolve(check) : reject()), 200);
    });
  }
  render() {
    const oneforth = Math.round(this.state.w / 8);
    const itemw = (this.state.w - oneforth) / this.state.letters.length;
    const marginTop =
      this.state.h / 2 - (itemw * this.state.letters.length) / 2;

    const locale = localeStrings[this.state.locale];

    return (
      <div
        style={{
          backgroundImage: `url("img/keyboard.png")`,
          height: this.state.h,
          width: this.state.w,
          textAlign: 'center',
        }}
        className="fullback vertcenter"
      >
        <div id="igradumiLevel">
          <Points score={this.state.score.num} />
          <div className="currentWord">
            {this.state.currentWord.map(letter => (
              <span
                className="letter react-transition swipe-down"
                key={letter.key}
              >
                {letter.letter.toUpperCase()}
              </span>
            ))}
          </div>
          {this.state.letters[0] && !this.state.levelFinished ? (
            <div>
              <div
                style={{
                  position: 'fixed',
                  width: this.state.w - oneforth,
                  height: this.state.w - oneforth,
                  top: marginTop,
                  left: oneforth / 2,
                }}
              >
                {this.state.dots.map((item, i) => (
                  <div
                    key={i}
                    style={{ float: 'left', width: itemw, height: itemw }}
                    className="vertcenter"
                  >
                    <div className="vertcenter1 dotItem">
                      <img src="./img/goldbtn.png" alt="" />
                      <div className="dotem">{item.toUpperCase()}</div>
                    </div>
                  </div>
                ))}
              </div>
              <PatternLock
                width={this.state.w - oneforth}
                pointSize={0}
                pointActiveSize={30}
                size={this.state.letters.length}
                connectorWidth={10}
                allowJumping={true}
                allowOverlapping={false}
                connectorRoundedCorners={true}
                errorColor="#c23616"
                style={{
                  position: 'fixed',
                  top: marginTop,
                  left: oneforth / 2,
                }}
                onChange={pattern => this.checkPattern(pattern)}
                onDotConnect={pattern => this.onDotConnect(pattern)}
              />
            </div>
          ) : (
            <div
              style={{
                width: this.state.w,
                height: this.state.h,
              }}
              className="vertcenter"
            >
              <div className="vertcenter1">
                <div className="lvlstart react-transition swipe-up">
                  <div className="plate">
                    <div className="lvlText">{locale.Level}</div>
                    <div className="lvl">{this.state.level.level}</div>
                  </div>
                </div>
                <img
                  onClick={() => this.startLevel()}
                  src="./img/coins/play.png"
                  alt=""
                  className="react-transition swipe-down pointer"
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Level;
