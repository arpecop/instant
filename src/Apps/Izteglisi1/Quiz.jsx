import React, { Component } from "react";
import { Grid, Header, Segment, Image } from "semantic-ui-react";

// import Script from 'react-load-script';
// import { FacebookLogin } from 'react-facebook-login-component';

import axios from "axios";
import shuffle from "lodash/shuffle";
import keys from "lodash/keys";

function mode(arr) {
  return arr
    .sort(
      (a, b) =>
        arr.filter(v => v === a).length - arr.filter(v => v === b).length
    )
    .pop();
}

export default class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: this.props.data._id,
      ordered:
        this.props.data.questions.length > 1
          ? shuffle(keys(this.props.data.results))
          : keys(this.props.data.results),
      appdomain: `https://apps.facebook.com/${this.props.fb.name}/${
        this.props.data._id
      }`,
      title: { display: "block", text: this.props.data.quiz.title },
      resultOccurance: this.props.data.questions.map(() => 0),
      personalResult: { id: 0, result: {} },
      resultimg: "",
      desc: { display: "block", text: this.props.data.quiz.desc },
      share: { display: "none", class: "ui facebook button" },
      type: this.props.type,
      triggerStyle: "ui facebook button",
      results: this.props.data.results,
      checked: this.props.data.questions.map(el =>
        this.props.data.results.map(elx => ({
          checked: false
        }))
      ),

      finalize: false
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.finalize;
  }

  responseFacebook() {
    this.setState({
      triggerStyle: "ui facebook button loading",
      finalize: true
    });

    axios
      .post("//grafix.herokuapp.com/tw", {
        id: this.state.personalResult.id,
        text: `${
          this.state.personalResult.result.short
            ? this.state.personalResult.result.short
            : ""
        }<br>${this.state.personalResult.result.full}`
      })
      .then(res => {
        const share = `https://www.izteglisi.club/${this.props.fb.name}/${
          this.props.data._id
        }result${this.state.personalResult.id}`;
        console.log(share);

        this.setState({
          resultimg: res.data.img,
          shareUrl: share,
          share: { display: "block", class: "ui facebook button" },
          desc: { display: "none" }
        });
      })
      .catch(error => {
        alert("error");
      });
  }
  handleClick = (i1, i) => {
    const resultOccurance = this.state.resultOccurance;
    resultOccurance[i1] = Math.round(this.state.ordered[i]);
    const modex = mode(resultOccurance);
    const personalResult = {
      result: this.state.results[modex],
      id: `${this.state._id}${modex}`
    };
    this.setState({ resultOccurance, personalResult });

    document.getElementById(`${i1}${i}`).checked = true;
  };

  render() {
    const childElements = this.props.data.questions.map((el, i1) => (
      <div key={i1} className="question">
        <div
          className="ui pointing below label"
          pointing="below"
          style={{
            fontWeight: "lighter",
            padding: "1rem",

            marginLeft: "auto",
            marginRight: "auto"
          }}
        >
          <small>
            {this.props.data.questions.length > 1
              ? `${i1 + 1}/${this.props.data.questions.length}`
              : ""}
          </small>{" "}
          {el.question}:
        </div>
        <div className="ui segments">
          {this.state.ordered.map((result, i) => (
            <div
              key={i}
              className="ui checked radio checkbox ui segment"
              onClick={e => this.handleClick(i1, i)}
            >
              <input
                id={`${i1}${i}`}
                className="hidden"
                name={`${i1}`}
                readOnly=""
                tabIndex="0"
                value={result}
                type="radio"
                checked={false}
              />
              <label>{`${el[Math.round(result) + 1]}   `}</label>
            </div>
          ))}
        </div>
      </div>
    ));
    return (
      <Grid centered>
        <Grid.Row>
          <Grid.Column width={12}>
            <div>
              <Header attached="top" />
              <Segment attached>
                <div
                  className="display1"
                  style={{ display: this.state.title.display }}
                >
                  <div>Направи този тест:</div>
                  <div
                    style={{ fontSize: "120%", textDecoration: "underline" }}
                  >
                    {this.state.title.text}
                  </div>
                </div>
                <div className="display2">{this.state.desc.text}</div>
              </Segment>
            </div>
            <form>{childElements}</form>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <div>
            <button
              style={{
                display: this.state.desc.display
              }}
              id="trigger"
              onClick={this.responseFacebook.bind(this)}
              // className="ui facebook button"
              className={this.state.triggerStyle}
            >
              {this.props.fb.login}
            </button>
            <Image rounded src={this.state.resultimg} />
            <button
              style={{ marginTop: 10, display: this.state.share.display }}
              appId={this.props.fb.appid}
              url={this.state.shareUrl}
              className={this.state.share.class}
            >
              {this.props.fb.share}
            </button>
          </div>
        </Grid.Row>
      </Grid>
    );
  }
}
