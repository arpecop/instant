import React from 'react';
import { Button } from 'antd';

export default class AsyncComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      resolvedError: false,
      resolvedSuccess: false,
      data: '',
    };
    this.renderChildren = this.renderChildren.bind(this);
  }

  componentDidMount() {
    this.props.promise().then(data => this.setState({ resolvedSuccess: true, data }));
  }

  renderChildren() {
    const { children } = this.props;
    const { data } = this.state;
    return React.Children.map(children, child => React.cloneElement(child, {
      data,
    }));
  }

  render() {
    const { resolvedSuccess, resolvedError } = this.state;
    if (resolvedError) {
      return <h1>Error Encountered</h1>;
    }
    if (resolvedSuccess) {
      return this.renderChildren();
    }
    return (
      <div>
        <Button type="primary" loading>
          Loading
        </Button>
      </div>
    );
  }
}
