import React from 'react';
import { Helmet } from 'react-helmet';
const metaTags = props => {
  const { meta } = props;
  return (
    <Helmet>
      <title>{meta.title}</title>
      <meta property="og:url" content={meta.url} />
      <meta property="og:title" content={meta.title} />
      <meta property="og:image" content={meta.image.src} />
      {meta.image.w ? (
        <React.Fragment>
          <meta property="og:image:width" content={meta.image.w} />
          <meta property="og:image:height" content={meta.image.h} />
        </React.Fragment>
      ) : (
        <React.Fragment />
      )}

      <meta property="og:type" content="article" />
      <meta property="og:description" content={meta.descr} />
    </Helmet>
  );
};
export default metaTags;
