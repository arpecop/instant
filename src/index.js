import React from 'react';
import ReactDOM from 'react-dom';

// @import "your-theme-file.less";
import { Router, Route } from 'react-router-dom';

import Main from './App';

const { createBrowserHistory } = require('history');

const customHistory = createBrowserHistory();
const App = () => (
     <Router history={customHistory}>
          <div>
               <Route path="/" exact component={() => <Main isIndex />} />

               <Route path="/:id/" exact render={props => <Main {...props} isIndex={false} />} />
               <Route path="/:id/:id2" exact render={props => <Main {...props} isIndex={false} />} />
               <Route path="/:id/:id2/:start_key" exact render={props => <Main {...props} isIndex={false} />} />
          </div>
     </Router>
);

// import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();
