
const localeStrings = {
    bg:{},
    en:{}
}
localeStrings.bg.Level = 'Ниво';
localeStrings.en.Level = 'Level'; 
localeStrings.bg.dumiEnter = 'Намерете скритата дума като свържете с провлачване всяка отделна буква, думите са минимум 4 букви.';
localeStrings.en.dumiEnter = 'Slide through the letters and find the hidden word , the words are minimum 4 letters';

export default localeStrings