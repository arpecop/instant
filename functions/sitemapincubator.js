/* eslint-disable func-names */
const https = require('https');

const bodybegin = `<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
<channel>
<title>...</title>
<link>https://www.netlify.com/blog/</link>
<description>...</description>
<generator>Hugo -- gohugo.io</generator>
<language>en-us</language>
<lastBuildDate>Fri, 15 Feb 2019 00:00:00 +0000</lastBuildDate>
<atom:link href="https://www.netlify.com/index.xml" rel="self" type="application/rss+xml"/>`;
const bodyend = `</channel>
</rss>`;
// https://arpecop.serveo.net/twitter/_design/api/_view/feed?limit=2000&reduce=true&update=false&group=true&start_key="a"
exports.handler = function (event, context, callback) {
  https.get(
    'https://arpecop.serveo.net/twitter/_design/api/_view/feed?limit=2000&reduce=false&update=false&group=false&descending=true',
    (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        const urls = JSON.parse(data).rows.map(
          item => `<item><link>https://incubator.netlify.com/u/${item.value.screenName}</link>
          <title>${item.value.screenName}</title>
          </item>
          `,
        );
        callback(null, {
          statusCode: 200,
          headers: { 'Content-Type': 'application/xml' },
          body: `${bodybegin}${urls.join('')}${bodyend}`,
        });
      });
    },
  );
};
